###################################################################
#
# FOSS License Compatibility Graph
#
# SPDX-FileCopyrightText: 2020 Henrik Sandklef
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
###################################################################
GRAPH_JSON=graph/license-compat.json

OUT_DIR=result
DOT_SUFFIX=gv
GRAPH_DOT=$(OUT_DIR)/license-compat.$(DOT_SUFFIX)

FORMATS := png pdf ps svg json fig jpg eps
VERSION=0.1

all: create

#
# Convert JSON to dot 
# - dep: the JSON file, outpur dir and the makefile
# 
$(GRAPH_DOT): $(OUT_DIR) $(GRAPH_JSON) Makefile
	@echo -n "Verifying JSON file: "
	@jq '.' $(GRAPH_JSON) >/dev/null 2>&1
	@echo "OK"

	@echo -n "Creating $@: "
	@bin/json2dot.py $(GRAPH_JSON) $(GRAPH_DOT)
	@echo "OK"

#
# a bit slow since it always reuilds - but hey, not that big of a deal
# - dep: the dot file (see above)
#
create: $(GRAPH_DOT)
	@echo "Creating misc formats"
	@for format in $(FORMATS); do \
		printf " * %-30s " "$(OUT_DIR)/license-compat.$${format}:" && \
		dot -T$${format} $(GRAPH_DOT)  > $(OUT_DIR)/license-compat.$${format} && \
		echo "OK" || exit ; \
	done; 

#
# Clean up all generated files
#
clean:
	@rm -fr ./$(OUT_DIR)

#
# Create outpur dir
#
$(OUT_DIR):
	@mkdir -p $(OUT_DIR)

test: create 

