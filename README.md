# FOSS License Compatibility Graph

FOSS License Compatibility Graph assists when checking FOSS components
for license compliance. The graph of FOSS license
compatibility we're providing is usable for both humans and machines.

* For humans we are graphical presentation of the licenses and their
compatibilities in various formats (e g png and pdf)

* For computers we provide a JSON file

With our graph over license compatibilities you or a computer can
quickly see if a software component is compatible with its
dependencies and their licenses. 

Here's the latest version of the graph:

![FOSS License Compatibility Graph - latest version](http://rameau.sandklef.com/flcg/license-compat.jpg  "FOSS License Compatibility Graph")

The project is hosted at gitlab.com
https://gitlab.com/sandklef/foss-license-compat-graph. 

# About the graph

We're using the same notation as did David Wheeler in his *The
Free-Libre / Open Source Software (FLOSS) License Slide*, available at
https://dwheeler.com/essays/floss-license-slide.html.

An arrow from a license or group to another, let's say *BSD-3-clause*
to *LGPL-2.1-only*, means you can combine softwares with these
licenses. We are not writing arrows between all the compatible
licenses of a component, since this would clutter down the graph with
arrows. Instead we're (mostly only) using arrows between licenses
where there is no other way to get to a license (by following the
arrows).

## Syntax and notations

The canonical source of the graph (in JSON) and can be
found in the `graph` directory in this project. The latest version is
available at gitlab.com:
https://gitlab.com/sandklef/foss-license-compat-graph/-/blob/master/graph/license-compat.json

### License

We're using the SPDX names (https://spdx.org/licenses/) for the licenses. 

A license specification could look like this:
```JSON
        {
            "spdx":"Apache-2.0",
            "used_by": [
                "LGPL-3.0-only",
                "LGPL-3.0-or-later"
            ]
        }
```

*spdx* - specifies the name (SPDX) of the license

*used_by* - which licenses can use this license. If you can follow the
 arrows from one license to another then you don't need to write an
 arrow between those.

### Group

A graph with all licenses will become unusable due to the huge amount
of arrows. We're trying to limit the amount by adding the concept of
groups where we put licenses having the same effect on other licenses.

As an example we have a group called *permissive* which (as of the
time when we wrote this page) contains the licenses *MIT* and
*X11*. The group is displayed as a license.

A group specification could look like this:
```JSON
        {
            "group_name":"permissive",
            "licenses": ["MIT" ,
                         "X11"],
            "used_by": [ "BSD-3-Clause"]
        }
```

*group_name* - specifies the name of the group

*licenses* - all licenses belonging to that group

*used_by* - same as for licenses

****Note:*** currently we don't show the licenses in a group in the graph for humans*

# Contribute to the project

## Add licenses to the graph

Our goal is to support as many licenses as possible and we need help
in adding licenses. If you would like to participate in this work,
feel free to contact us (see contact details below).

## Review 

We need help reviewing the compatibility graph. If you find any bugs,
report them at
https://gitlab.com/sandklef/foss-license-compat-graph/-/issues

## Contact us

Get in touch with us via email flcg _-AT-_ sandklef.com (flcg as in
the acronym FOSS License Compatibility Graph).

# Edit and build the graph

## Required tools

* Make (GNU)

* Graphviz

* Python

* jq

### Debian and Ubuntu users

```
sudo apt-get install graphviz jq make
```

### Fedora and Redhat users

```
sudo yum install graphviz jq make
```

## Building

To build all supported formats:

~~~
make
~~~

You'll find the created graphs in various formats in the `result`
folder.

# Programs using the graph

## FOSS License Compliance Tool (flict)

flict reads a component specified in a separate (JSON) file. Together
with a license compatibility graph (JSON) the tool can check if the
component and its depencies (themselves being components) is
compliant.

Read more at: https://github.com/vinland-technology/flict
