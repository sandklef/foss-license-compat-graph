#!/usr/bin/python3

###################################################################
#
# FOSS License Compatibility Graph
#
# SPDX-FileCopyrightText: 2020 Mikko Hellsing, Henrik Sandklef
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
###################################################################

import json
import os
import sys

here = "."
#os.path.abspath(os.path.dirname(__file__))

def main(inpath, outpath):
  with open(os.path.join(here, inpath)) as fp:
    compats = json.load(fp)["compatibilities"]
  with open(os.path.join(here, outpath), "w") as fp:
    fp.write("digraph depends {\n")
    fp.write("    node [shape=plaintext]\n")
    for compat in compats:
#      print("compat: ", compat)
      if "used_by" in compat:
        for attr in ["spdx", "group_name"]:
#          print("  attr: ", attr)
          if attr in compat:
#            print("     attr in compat")
            for used_by in compat["used_by"]:
#              print("        used_by: ", used_by)
              fp.write(f"    \"{compat[attr]}\" -> \"{used_by}\"\n")
    fp.write("}")

if __name__ == "__main__":
  main(sys.argv[1], sys.argv[2])

